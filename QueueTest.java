// Data Structures & Algorithms
// Spring 2018
// testing generic stacks


public class QueueTest {

    public static void main(String[] args)  {
        Integer[] numbers = {5, 16, 8};
        queueTest(numbers);
        String[] strings = {"alpha", "beta", "gamma"};
        queueTest(strings);
        timeQueue();
    }

    public static <T> void queueTest(T[] items) {
        QueueInterface<T> q = new ArrayListQueue<T>();
        for (T x : items) {
            System.out.println("queuing: " + x);
            q.enqueue(x);
        }
        while (!q.isEmpty()) {
            System.out.println("dequeuing: " + q.dequeue());
        }
    }

    public static void timeQueue() {
        final long MIN_TIME = 10000000; // 10^7
        QueueInterface<Boolean> q = new ArrayListQueue<Boolean>();
        long startTime = System.nanoTime();
        long t = 0;
        long n = 0;
        do {
            q.enqueue(true);
            n++;
            t = System.nanoTime() - startTime;
        } while (t < MIN_TIME);
        long avg = t / n;
        System.out.println("" + n + " items queued, average time: " + avg + "ns");
        startTime = System.nanoTime();
        while (!q.isEmpty()) {
            q.dequeue();
        }
        t = System.nanoTime() - startTime;
        avg = t / n;
        System.out.println("avg dequeuing time: " + avg + "ns");
    }

}
