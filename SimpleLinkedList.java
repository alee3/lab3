/**
 * simplified generic linked lists
 * 
 * @author <Anthony & Katie>
 */
public class SimpleLinkedList<T> implements SimpleListInterface<T>
{
    private class Node{
        public T value;
        public Node next;
        public Node(T value, Node next) 
        {
            this.value = value;
            this.next = next;
        }
    }

    private Node _head;

    
    /**
     * Constructs an empty list.
     */
    public SimpleLinkedList() 
    {
        _head = null;
    }

    
    /**
     * Returns number of elements on list.
     * @return number of elements on list
     */
    public int size() {
        int n = 0;
        for(Node node = _head; node != null; node = node.next)
        {
            n = n+1;
        }
        return n;
    }

    
    /**
     * Returns element at specified position on list.
     * @param index position of element to return
     * @return element at specified position on list
     * @throws IndexOutOfBoundsException - if index < 0 or >= size()
     */
    public T get(int index) 
    {
        int counter = 0;
        if (index < 0)
        {
          throw new IndexOutOfBoundsException("index: " + index);
        } 
        
        for(Node node = _head; node != null; node = node.next)
        {
            if(counter == index)
            {
                return node.value;
            }
            counter += 1;
        }
        
        return null;
       
        // ...
    }

    
    /**
     * Replaces whatever was at specified position on list with
     * specified element.
     * @param index position of element to return
     * @param element to put into list
     */
    public void set(int index, T element) {
        int counter = 0;
        if (index < 0 || index >= size())
        {
          throw new IndexOutOfBoundsException("index: " + index);
        } 
        
        for(Node node = _head; node != null; node = node.next)
        {
            if(counter == index)
            {
                node.value = element;
            }
            counter += 1;
        }
        
        
    }

    
    /**
     * Appends new element onto back of list.
     * @param element to appended onto list
     */
    public void append(T element) 
    {
        if (_head == null)  
        {
            _head = new Node(element, null);
        }
        else
        {
            Node node = _head;
            while (node.next != null)
            {
                node = node.next;
            }
            node.next = new Node(element,null);
        }
    }        

    
    /**
     * Returns position of element in list if present, otherwise returns
     * -1.
     * @param element item to be searched for
     * @return position of element in list or -1 if not found
     */
    public int indexOf(T element) 
    {
        int pos = 0;
        for(Node node = _head; node !=null; node = node.next)
        {
            if (node.value.equals(element))
            {
                return pos;
            }
            pos += 1;
        }
        return -1;
    }

    
    /**
     * Returns string representation of list.
     * @return string representation of list
     */
    public String toString() {
        String s = "[";
        for(Node node = _head; node != null; node = node.next)
        {
            if(node.next == null)
            {
                s = s + node.value;
            }
            else
            {
                s = s + node.value + ", ";
            }
        }
        s += "]";
        return s;
    }


    // ...
}
