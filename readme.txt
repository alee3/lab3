Data Structures & Algorithms - Spring 2018
==========================================

## Lab 2: Links and Queues
### Monday, February 5

-----------------------------------------------------------------

### Main tasks for lab

0. Examine all the files in the starter archive. Run StackTest.main to
   verify that generic stacks work as expected, and to get the gist of
   basic performance measurement. Which seems faster: pushing or popping?
   Experiment with the MIN_TIME parameter in StackTest.timeStack.

1. Examine QueueInterface more closely. Complete ArrayListQueue so it
   matches that interface and it works with QueueTest. Which seems
   faster: inserting items onto the queue or removing them? Experiment
   with the MIN_TIME parameter in QueueTest.timeQueue.

2. Examine SimpleListInterface more closely. Complete SimpleLinkedList
   so it matches that interface and it works with ListTest. If two
   methods use very similar ideas, write a private helper method to
   avoid redundancy. Why does the last part of the last test - where an
   empty list should be found inside the outer list not work? Why for
   the list of list of integers is the type declared as:
   
        SimpleLinkedList<SimpleListInterface<Integer>>
        
    rather than as:
    
        SimpleListInterface<SimpleListInterface<Integer>>
    
    or some other way?


-----------------------------------------------------------------

### Time permitting in lab
#### (otherwise complete on your own time)

3. Add an equals method to SimpleListInterface and SimpleLinkedList so
   that the final empty-list lookup test in ListTest works.

4. Add methods to the SimpleListInterface and SimpleLinkedList for
   inserting and removing an item at a given position. Add test code to
   ListTest.

5. Add a "copy constructor" to SimpleLinkedList that creates a new list
   that is a copy of the supplied list. Think about what you implement
   is a deep or shallow copy; add test code to ListTest.
   
6. Reimplement ArrayListQueue more efficiently.
