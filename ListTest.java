// Data Structures & Algorithms
// Spring 2018
// testing simple generic linked lists


public class ListTest {

    public static void main(String[] args) {
        testIntList();
        System.out.println();
        //testStringList();
        //System.out.println();
        //testIntListList();
    }

    public static void testIntList() {
        final int N = 5;
        final int P = N / 2;
        SimpleListInterface<Integer> ints = new SimpleLinkedList<Integer>();
        System.out.println("size: " + ints.size() + "; contents: " + ints);
        for (int i = 0; i < N; i++) {
            ints.append(2 * i);
        }
        int x = ints.get(P);
        ints.set(P, x - 1);
        System.out.println("size: " + ints.size() + "; contents: " + ints);
        System.out.println("position of " + x + " is " + ints.indexOf(x));
        System.out.println("position of " + (x-1) + " is " + ints.indexOf(x-1));
    }                
                           
    public static void testStringList() {
        String[] a = { "alpha", "beta", "gamma", "delta", "epsilon" };
        final int P = a.length / 2;
        SimpleListInterface<String> strings = new SimpleLinkedList<String>();
        System.out.println("size: " + strings.size() + "; contents: " + strings);
        for (String s : a) {
            strings.append(s);
        }
        String x = strings.get(P);
        String xBang = x + "!";
        strings.set(P, xBang);
        System.out.println("size: " + strings.size() + "; contents: " + strings);
        System.out.println("position of \"" + x + "\" is " + strings.indexOf(x));
        System.out.println("position of \"" + xBang + "\" is " + strings.indexOf(xBang));
    }                

    public static void testIntListList() {
        final int N = 5;
        SimpleLinkedList<SimpleListInterface<Integer>> intLists = 
            new SimpleLinkedList<SimpleListInterface<Integer>>();
        System.out.println("size: " + intLists.size() + "; contents: " + intLists);
        for (int i = 0; i < N; i++) {
            SimpleListInterface<Integer> ints = new SimpleLinkedList<Integer>();
            for (int j = 0; j <= i; j++) {
                ints.append(2 * j);
            }
            intLists.append(ints);
        }
        System.out.println("size: " + intLists.size() + "; contents: " + intLists);

        // modifying the element within the element:
        SimpleListInterface<Integer> ints = intLists.get(1);
        ints.set(1, -ints.get(1));

        // replacing an entire sublist
        ints = new SimpleLinkedList<Integer>();
        for (int i = 0; i < N; i++) {
            ints.append(i);
        }
        intLists.set(2, ints);
        System.out.println("size: " + intLists.size() + "; contents: " + intLists);

        // finding that exact list:
        System.out.println("position of " + ints + " is " + intLists.indexOf(ints));
        // as usual, not finding something not there:
        ints = new SimpleLinkedList<Integer>();
        System.out.println("position of " + ints + " is " + intLists.indexOf(ints));

        intLists.append(ints);
        System.out.println("size: " + intLists.size() + "; contents: " + intLists);
        // but without true equals test...
        ints = new SimpleLinkedList<Integer>();  // a different empty list!
        System.out.println("position of " + ints + " is " + intLists.indexOf(ints));
    }                

}
