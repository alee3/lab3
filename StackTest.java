// Data Structures & Algorithms
// Spring 2018
// testing generic stacks


public class StackTest {

    public static void main(String[] args)  {
        Integer[] numbers = {5, 16, 8};
        stackTest(numbers);
        String[] strings = {"alpha", "beta", "gamma"};
        stackTest(strings);
        timeStack();
    }

    public static <T> void stackTest(T[] items) {
        StackInterface<T> stack = new ArrayListStack<T>();
        for (T x : items) {
            System.out.println("pushing: " + x);
            stack.push(x);
        }
        while (!stack.isEmpty()) {
            System.out.println("popping: " + stack.pop());
        }
    }

    public static void timeStack() {
        final long MIN_TIME = 900000000; // 9x10^8
        StackInterface<Boolean> stack = new ArrayListStack<Boolean>();
        long startTime = System.nanoTime();
        long t = 0;
        long n = 0;
        do {
            stack.push(true);
            n++;
            t = System.nanoTime() - startTime;
        } while (t < MIN_TIME);
        long avg = t / n;
        System.out.println("" + n + " items pushed, average time: " + avg + "ns");
        startTime = System.nanoTime();
        while (!stack.isEmpty()) {
            stack.pop();
        }
        t = System.nanoTime() - startTime;
        avg = t / n;
        System.out.println("avg popping time: " + avg + "ns");
    }

}
